﻿using NoroffAssignment.Model.Money;
using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.Interface
{
    interface IController
    {
        bool Cash(List<Currency> amount);
        bool Debit(double credit, string cardNumber, string expiration, int cvc);
        bool Credit(double credit, string cardNumber, string expiration, int cvc);
    }
}
