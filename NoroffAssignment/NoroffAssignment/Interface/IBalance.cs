﻿using NoroffAssignment.Model.Money;
using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.Interface
{
    interface IBalance
    {
        double Debit { get; set; }
        double Credit { get; set; }
        List<Currency> Cash { get; }

        double TotalBalance();
        void PayDebit(double amount);
        void PayCredit(double amount);
    }
}
