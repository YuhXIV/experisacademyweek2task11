﻿using NoroffAssignment.Interface;
using NoroffAssignment.Model.Money;
using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.Model
{
    class Controller : IController
    {
        readonly double amount = 501;
        readonly Balance balance;
        /// <summary>
        /// On creation of controller, all the necessary prompt will run
        /// </summary>
        public Controller()
        {
            List<Currency> cash = new List<Currency>();
            cash = GenMoney(cash);
            balance = new Balance(cash, 600, 1000);
            Console.WriteLine($"You're in the store buying some stuff for {amount}");
            Console.WriteLine("Choose payment method: Cash[1]  Debit card[2]  Credit card[3]");
            bool transactionSucceed = false;
            do
            {
                string input = Console.ReadLine();
                int choice;
                bool validChoice = int.TryParse(input, out choice);
                if (!validChoice || choice > 3 || choice == 0)
                {
                    Console.WriteLine("Invalid choice");
                }
                else
                {
                    if (choice == 1) transactionSucceed = Cash(balance.Cash);
                    else if (choice == 2) transactionSucceed = Debit(balance.Debit, GenCardNumber(), GenExpirationDate(), new Random().Next(100, 999));
                    else if (choice == 3) transactionSucceed = Credit(balance.Credit, GenCardNumber(), GenExpirationDate(), new Random().Next(100, 999));
                }

            } while (!transactionSucceed);
            Console.WriteLine("Thanks for your purchase");
        }

        /// <summary>
        /// Make the transaction with cash, usually the balance is not displayed, but is displayed
        /// to show that a transaction has happened.
        /// </summary>
        /// <param name="credit"></param>
        /// <returns></returns>
        public bool Cash(List<Currency> credit)
        {
            Console.WriteLine($"Your cash balance: {balance.GetAllCash()}");
            Cash cash = new Cash(amount);
            bool transaction = cash.Transaction(credit);
            if(transaction)
            {
                balance.PayCash(amount);
                cash.Payed = true;
                Console.WriteLine($"Transaction succeeded, your new cash balance is: {balance.GetAllCash()}");
                return true;
            } 
            Console.WriteLine("Transaction failed, no change to your cash balance");
            return false;
        }

        /// <summary>
        /// Make the transaction with debit card, usually the balance is not displayed, but is displayed
        /// to show that a transaction has happened.
        /// </summary>
        /// <param name="credit"></param>
        /// <param name="cardNumber"></param>
        /// <param name="expiration"></param>
        /// <param name="cvc"></param>
        /// <returns></returns>
        public bool Debit(double credit, string cardNumber, string expiration, int cvc)
        {
            Console.WriteLine($"Your cash balance: {balance.Debit}");
            
            Debit debit = new Debit(amount, cardNumber, expiration, cvc);
            bool transaction = debit.Transaction(credit);
            if (transaction)
            {
                balance.PayDebit(amount);
                debit.Payed = true;
                Console.WriteLine($"Transaction succeeded, your new cash balance is: {balance.Debit}");
                return true;
            }
            Console.WriteLine("Transaction failed, no change to your cash balance");
            return false;
        }
        /// <summary>
        /// Make the transaction with credit card, usually the balance is not displayed, but is displayed
        /// to show that a transaction has happened.
        /// </summary>
        /// <param name="credit"></param>
        /// <param name="cardNumber"></param>
        /// <param name="expiration"></param>
        /// <param name="cvc"></param>
        /// <returns></returns>
        public bool Credit(double credit, string cardNumber, string expiration, int cvc)
        {
            Console.WriteLine($"Your cash balance: {balance.Credit}");

            Credit creditCard = new Credit(amount, cardNumber, expiration, cvc);
            bool transaction = creditCard.Transaction(credit);
            if (transaction)
            {
                balance.PayCredit(amount);
                creditCard.Payed = true;
                Console.WriteLine($"Transaction succeeded, your new cash balance is: {balance.Credit}");
                Console.WriteLine("Please write your signature:");
                do
                {
                    creditCard.Signature = Console.ReadLine();
                    if(creditCard.Signature.Length > 0)
                    {
                        break;
                    }
                    Console.WriteLine("Please enter atleast 1 character");
                } while (true);

                return true;
            }
            Console.WriteLine("Transaction failed, no change to your cash balance");
            return false;
        }

        /// <summary>
        /// Exist only to make it working in console, in a real life a card number will be given
        /// by the custommer when card is used.
        /// </summary>
        /// <returns></returns>
        public string GenCardNumber()
        {
            Random rand = new Random();
            var builder = new StringBuilder();
            while (builder.Length < 16)
            {
                builder.Append(rand.Next(10).ToString());
            }
            return builder.ToString();
        }
        /// <summary>
        /// Generate a expiration date for the cards, in a real life a card number will be given
        /// by the custommer when card is used
        /// </summary>
        /// <returns></returns>
        public string GenExpirationDate()
        {
            Random rand = new Random();
            var builder = new StringBuilder();
            builder.Append(rand.Next(1,12).ToString() + "/20");
            return builder.ToString();
        }
        /// <summary>
        /// The customer usually has controll over their own money pouch, but for the console to work,
        /// generating a money pouch for the user to run it smoothly
        /// </summary>
        /// <param name="cash"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<Currency> GenMoney(List<Currency> cash)
        {
            while(cash.Count < 20)
            {
                cash.Add(new OneCoin());
                cash.Add(new TenCoin());
                cash.Add(new HundredNote());
                cash.Add(new FiftyNote());
            }
            return cash;
        }
    }
}
