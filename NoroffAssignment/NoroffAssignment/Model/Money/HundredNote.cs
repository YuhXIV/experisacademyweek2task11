﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.Model.Money
{
    class HundredNote : Note
    {
        public HundredNote() : base()
        {
            Value = 100;
        }
    }
}
