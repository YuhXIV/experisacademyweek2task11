﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.Model.Money
{
    abstract class Currency
    {
        public Currency()
        {
            CurrencyType = "kr";
        }
        public string CurrencyType { get; }
        public int Value { get; set; }
    }
}
