﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.Model.Money
{
    class OneCoin : Coin
    {
        public OneCoin() : base()
        {
            Value = 1;
        }
    }
}
