﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.Model.Money
{
    class FiftyNote : Note
    {
        public FiftyNote() : base()
        {
            Value = 50;
        }
    }
}
