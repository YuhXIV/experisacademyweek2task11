﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.Model.Money
{
    class TenCoin : Coin
    {
        public TenCoin() : base()
        {
            Value = 10;
        }
    }
}
