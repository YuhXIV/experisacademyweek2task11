﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.Model
{
    class Cash : Payment
    {
        public Cash(double amount) : base(amount)
        {

        }

        /// <summary>
        /// Round up the the number to whole numbers as cash (kr) does not have decimal number.
        /// </summary>
        /// <param name="cash"></param>
        /// <returns></returns>
        public int Change (int cash)
        {
            return (int)Math.Round(cash - Amount);
        }
    }
}
