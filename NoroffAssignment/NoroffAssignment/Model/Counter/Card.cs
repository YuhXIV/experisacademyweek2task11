﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.Model
{
    abstract class Card : Payment
    {
        public Card(double amount, string cardNumber, string expirationDate, int cvc) : base(amount)
        {
            CardNumber = cardNumber;
            ExpirationDate = expirationDate;
            Cvc = cvc;
        }
        public string CardNumber { get; set; }
        public string ExpirationDate { get; }
        public int Cvc { get; }
    }
}
