﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.Model
{
    class Credit : Card
    {
        public Credit(double amount, string cardNumber, string expirationDate, int cvc) : base(amount, cardNumber, expirationDate, cvc)
        {
            
        } 
        public string Signature { get; set; }
    }
}
