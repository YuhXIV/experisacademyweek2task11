﻿using NoroffAssignment.Model.Money;
using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.Model
{
    abstract class Payment
    {
        public Payment(double amount)
        {
            Amount = amount;
            Payed = false;
        }

        public double Amount { get; set; }

        public bool Payed{ get; set; }

        /// <summary>
        /// Check if the balance of cards is higher than the amount, if balance is lower means the customer does not
        /// have enought balance to pay it
        /// </summary>
        /// <param name="balance"></param>
        /// <returns></returns>
        public bool Transaction(double balance)
        {
            if (balance >= Amount) return true;
            return false;
        }
        /// <summary>
        /// Check if the balance of cash is higher than the amount, if balance is lower means the customer does not
        /// have enought balance to pay it
        /// </summary>
        /// <param name="balance"></param>
        /// <returns></returns>
        public bool Transaction(List<Currency> balance)
        {
            int total = 0;
            foreach(Currency c in balance)
            {
                total += c.Value;
            }
            if (total > Amount) return true;
            return false;
        }
    }
}
