﻿using NoroffAssignment.Interface;
using NoroffAssignment.Model.Money;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace NoroffAssignment.Model
{
    class Balance : IBalance
    {
        public Balance(List<Currency> cash, double debit, double credit)
        {
            Debit = debit;
            Credit = credit;
            Cash = cash;
        }
        public double Debit { get; set; }
        public double Credit { get; set; }
        public List<Currency> Cash { get; set; }

        /// <summary>
        /// Return the total amount of the objects money balance
        /// </summary>
        /// <returns></returns>
        public double TotalBalance()
        {
            return Debit + Credit + GetAllCash();
        }

        /// <summary>
        /// Returns the sum amount of all the cash balance
        /// </summary>
        /// <returns></returns>
        public int GetAllCash()
        {
            int cash = 0;
            foreach (Currency c in Cash)
            {
                cash += c.Value;
            }
            return cash;
        }
        /// <summary>
        /// substraction the amount from the sum of the debit card balance
        /// </summary>
        /// <param name="amount"></param>
        public void PayDebit(double amount)
        {
            Debit -= amount;
        }
        /// <summary>
        /// substraction the amount from the sum of the credti card balance
        /// </summary>
        /// <param name="amount"></param>
        public void PayCredit(double amount)
        {
            Credit -= amount;
        }
        /// <summary>
        /// substraction the amount from the sum of the cash balance
        /// </summary>
        /// <param name="amount"></param>
        public void PayCash(double amount)
        {
            Cash.OrderBy( currency => currency.Value).ToList();
            while(true)
            {
                if (amount > 0 && Cash.Count > 0)
                {
                    amount -= Cash.ElementAt(0).Value;
                    Cash.Remove(Cash.ElementAt(0));
                }
                else break;
            }
            ReturnChange(-((int)Math.Round(amount)));
        }
        /// <summary>
        /// Method to return the changes to the customer, starting with the biggest currency value.
        /// </summary>
        /// <param name="change"></param>
        public void ReturnChange(int change)
        {
            while (change > 0)
            {
                if (change > 100)
                {
                    Cash.Add(new HundredNote());
                    change -= 100;
                }
                else if (change >= 50 && change < 100)
                {
                    Cash.Add(new FiftyNote());
                    change -= 50;
                }
                else if (change >= 10 && change < 50)
                {
                    Cash.Add(new TenCoin());
                    change -= 10;
                }
                else if (change < 10)
                {
                    Cash.Add(new OneCoin());
                    change -= 1;
                }
            }
        }
    }
}
